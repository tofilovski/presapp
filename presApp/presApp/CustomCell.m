//
//  CustomCell.m
//  presApp
//
//  Created by Miroslav Pavlovski on 5/8/14.
//
//

#import "CustomCell.h"

@implementation CustomCell
- (id)initWithFrame:(CGRect)frame
{
    //self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
}
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)playBtnAction:(id)sender
{
    NSLog(@"PLAY THE PRESENTATION");
    //playPresentation
    //performSegueWithIdentifier:@"playPresentation" sender:self;
}

- (IBAction)editBtnAction:(id)sender
{
    NSLog(@"EDIT THE PRESENTATION");
}

- (IBAction)emailBtnAction:(id)sender
{
    NSLog(@"EMAIL THE PRESENTATION");
}

- (IBAction)deleteBtnAction:(id)sender
{
    NSLog(@"DELETE THE PRESENTATION");
}

@end

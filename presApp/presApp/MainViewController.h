//
//  MainViewController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import <UIKit/UIKit.h>
#import "PopContentController.h"
#import "SubViewTest.h"
#import "AllPresentations.h"
#import "MakePresentation.h"


@interface MainViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>
{
    IBOutlet UIButton *settingsBtn;
    IBOutlet UIButton *calendarBtn;
    IBOutlet UIButton *calcBtn;
    IBOutlet UIButton *programsBtn;
    
}
@property (nonatomic, strong) UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
@property (strong, nonatomic) IBOutlet UIButton *calendarButton;
@property (strong, nonatomic) IBOutlet UIButton *calculatorButton;
@property (strong, nonatomic) IBOutlet AllPresentations *collectionView;
@property (strong, nonatomic) IBOutlet MakePresentation *makePresentation;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIView *cellMenuView;
@property (strong, nonatomic) IBOutlet UIScrollView *dropArea;

@property (strong, nonatomic) IBOutlet UIImageView *introSlideView;
@property (strong, nonatomic) IBOutlet UIImageView *slideOneView;
@property (strong, nonatomic) IBOutlet UIImageView *slideTwoView;
@property (strong, nonatomic) IBOutlet UIImageView *slideThreeView;
@property (strong, nonatomic) IBOutlet UIImageView *slideFourView;
@property (strong, nonatomic) IBOutlet UIImageView *slideFiveView;
@property (strong, nonatomic) IBOutlet UIImageView *emailSlideView;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *previewBtn;

//@property (strong, nonatomic) IBOutlet SubViewTest *programVIew;
//@property (strong, nonatomic) IBOutlet AllPresentations *allPresentation;
@property (strong, nonatomic) IBOutlet UICollectionView *assetCollectionView;
@property (nonatomic, strong) UIPopoverController *popOver;
//@property (nonatomic, strong) UIButton *button;
//@property (strong, nonatomic) IBOutlet UIButton *popButton;
- (IBAction)changeViews:(id)sender;

- (IBAction)saveBtnAction:(id)sender;
- (IBAction)previewBtnAction:(id)sender;

- (IBAction)openPopUp:(id)sender;
//- (IBAction)removeView:(id)sender;
- (IBAction)pageSelected:(id)sender;
@end

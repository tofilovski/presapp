//
//  SignUpScreenController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/9/14.
//
//

#import "SignUpScreenController.h"
#import "QuartzCore/QuartzCore.h"
#import "Parse/Parse.h"

@interface SignUpScreenController ()

@end

@implementation SignUpScreenController

@synthesize nameEntered;
@synthesize passwordEntered;
@synthesize emailEntered;
@synthesize username, password, passwordChk, email;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"####################");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIDeviceOrientation orientation = self.interfaceOrientation;    
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
    
    username.text = @"";
    password.text = @"";
    passwordChk.text = @"";
    email.text = @"";
    
    UIColor * colorField = [UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0f];
    [[username layer] setBorderWidth:1.5f];
    [[username layer] setBorderColor:colorField.CGColor];
    
    [[password layer] setBorderWidth:1.5f];
    [[password layer] setBorderColor:colorField.CGColor];
    
    [[passwordChk layer] setBorderWidth:1.5f];
    [[passwordChk layer] setBorderColor:colorField.CGColor];
    
    [[email layer] setBorderWidth:1.5f];
    [[email layer] setBorderColor:colorField.CGColor];
    
    
    UIColor * colorBtn = [UIColor colorWithRed:141/255.0f green:179/255.0f blue:68/255.0f alpha:1.0f];
    [[signUpBtn layer] setBorderWidth:1.5f];
    [[signUpBtn layer] setBorderColor:colorBtn.CGColor];
    
    [[cancelBtn layer] setBorderWidth:1.5f];
    [[cancelBtn layer] setBorderColor:colorBtn.CGColor];

    
    /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    NSLog(@"documentPath to read from   %@  ", documentPath);
    NSString *credList = [documentPath stringByAppendingPathComponent:@"credentialsList.plist"];
    NSLog(@"File to read from 1  %@  ", credList);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:credList])
    {
        //credList = [[NSBundle mainBundle] pathForResource:@"credentialsList" ofType:@"plist"];
        credList = @"/Users/boban/Library/Application Support/iPhone Simulator/6.0/Applications/03C5F053-1607-46FC-82C3-D490C660CB01/Documents/credentialsList.plist";
    }
    
    NSLog(@"File to read from  2 %@  ", credList);
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:credList];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistXML mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&errorDesc];
    
    if (!temp)
    {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }
    
    self.nameEntered = [temp objectForKey:@"userName"];
    self.passwordEntered = [temp objectForKey:@"passWord"];
    self.emailEntered = [temp objectForKey:@"userEmail"];
    
    username.text = nameEntered;
    password.text = passwordEntered;
    email.text = emailEntered;
     
     */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
}

- (IBAction)signUpBtn:(id)sender
{
    NSString *fileName = @"credentialsList.plist";//credentialsList.plist
    NSLog(@"1");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"2");
    NSString *docPath = [paths objectAtIndex:0];
    NSLog(@"3 %@   ", docPath);
    NSString *credList = [docPath stringByAppendingPathComponent:fileName];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL checkFile = [fileMgr fileExistsAtPath:credList];
    NSLog(@"%@", (checkFile ? @"exists": @"notExist"));    
    NSLog(@"4");
    self.nameEntered = username.text;
    if ([password.text isEqualToString:passwordChk.text])
    {
        NSLog(@"5");
        NSLog(@"5  %@   kako  %@  ", password.text, passwordChk.text);
        self.passwordEntered = password.text;
    }
    else
    {
        NSLog(@"6  %@   kako  %@  ", password.text, passwordChk.text);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Difference in password" message:@"The paswords entered don't match" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSCharacterSet *CharSet = [NSCharacterSet characterSetWithCharactersInString:@"@"];
    NSRange range = [email.text rangeOfCharacterFromSet:CharSet];
    if (range.location == NSNotFound)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect email" message:@"This email address is incorrect " delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else
    {
        NSLog(@"7");
        self.emailEntered = email.text;
    }
    
    //NSLog(@"7");
    //self.emailEntered = email.text;
    NSLog(@"8");
    NSDictionary *plistDict = [NSDictionary dictionaryWithObjects: [ NSArray arrayWithObjects: nameEntered, passwordEntered, emailEntered, nil] forKeys:[ NSArray arrayWithObjects: @"userName", @"passWord", @"userEmail", nil]];
    NSLog(@"9");
    NSString *error = nil;
    NSLog(@"10");
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    NSLog(@"11 %@      %@      %@", [plistDict valueForKey:@"userName"], [plistDict valueForKey:@"passWord"], [plistDict valueForKey:@"userEmail"]);
    //credList = @"/Users/boban/Library/Application Support/iPhone Simulator/6.0/Applications/03C5F053-1607-46FC-82C3-D490C660CB01/presApp.app/credentialsList.plist";
    
    if (plistData)
    {
        NSLog(@"Zapis %@", credList);
        [plistData writeToFile:credList atomically:YES];
        
    }
    else
    {
        NSLog(@"Error in saveDataL %@", error);
    }
    
    PFUser *user = [PFUser user];
    user.username = nameEntered;
    user.password = passwordEntered;
    user.email = emailEntered;
    
    [user signUpInBackgroundWithTarget:self selector:@selector(handleSignUp:error:)];
}
- (IBAction)cancelSignUp:(id)sender
{
    username.text = @"";
    password.text = @"";
    passwordChk.text = @"";
    email.text = @"";
    
    nameEntered = @"";
    passwordEntered = @"";
    emailEntered = @"";
    
    [self performSegueWithIdentifier:@"signUpToLogin" sender:self];
}

- (void)handleSignUp:(NSNumber *)result error:(NSError *)error
{
    if (!error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign up succesfull!" message:@"Login and enjoy" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        [self performSegueWithIdentifier:@"signUpToLogin" sender:self];
    }
    else
    {
        NSString *errorString = [error userInfo][@"error"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign up not succesfull!" message:errorString delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
    }
}
@end

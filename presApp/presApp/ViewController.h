//
//  ViewController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/8/14.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) NSTimer *countDown;
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;


@end

//
//  SecondViewController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (nonatomic, strong) UIButton *menuBtn;

@end

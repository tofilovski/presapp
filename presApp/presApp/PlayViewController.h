//
//  PlayViewController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 5/16/14.
//
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PlayViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    MPMoviePlayerController *videoPlayer;
    NSURL *videoURL;
}

@property (strong, nonatomic) IBOutlet UILabel *presentationNameLabel;
@property (strong, nonatomic) NSString *presentationName;
@property (strong, nonatomic) IBOutlet UILabel *presentationDateLabel;
@property (strong, nonatomic) NSString *presentationDate;
@property (strong, nonatomic) IBOutlet UIImageView *presentationImageView;
@property (strong, nonatomic) UIImage *presentationImage;

- (IBAction)playButton:(id)sender;

//- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               //usingDelegate: (id <UIImagePickerControllerDelegate,
                                               //UINavigationControllerDelegate>) delegate;


@end

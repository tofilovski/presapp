//
//  MakePresentation.h
//  presApp
//
//  Created by Miroslav Pavlovski on 5/9/14.
//
//

#import <UIKit/UIKit.h>

@interface MakePresentation : UIView <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IBOutlet UITextField *presentationName;
@property (nonatomic, strong) IBOutlet UITextField *presentationDate;
@property (nonatomic, strong) IBOutlet UITextField *presentationTime;

@property (nonatomic, strong) IBOutlet UICollectionView *assetCollection;
@end

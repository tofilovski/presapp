//
//  SignUpScreenController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/9/14.
//
//

#import <UIKit/UIKit.h>

@interface SignUpScreenController : UIViewController
{
    IBOutlet UITextField *username;
    IBOutlet UITextField *password;
    IBOutlet UITextField *passwordChk;
    IBOutlet UITextField *email;
    IBOutlet UIButton *signUpBtn;
    IBOutlet UIButton *cancelBtn;
}

@property (nonatomic, strong) UITextField *username;
@property (nonatomic, strong) UITextField *password;
@property (nonatomic, strong) UITextField *passwordChk;
@property (nonatomic, strong) UITextField *email;

@property (nonatomic, strong) NSString *nameEntered;
@property (nonatomic, strong) NSString *passwordEntered;
@property (nonatomic, strong) NSString *emailEntered;

- (IBAction)signUpBtn:(id)sender;

@end

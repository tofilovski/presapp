//
//  LoginScreenController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/8/14.
//
//

#import <UIKit/UIKit.h>

@interface LoginScreenController : UIViewController{
    IBOutlet UITextField *userName;
    IBOutlet UITextField *passWord;
    IBOutlet UIButton *logInBtn;
    IBOutlet UIButton *signUpBtn;
}

- (IBAction)Login:(id)sender;
- (IBAction)SignUp:(id)sender;

@end

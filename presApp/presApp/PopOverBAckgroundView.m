//
//  PopOverBAckgroundView.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/25/14.
//
//

#import "PopOverBAckgroundView.h"

@implementation PopOverBAckgroundView

@synthesize arrowDirection = _arrowDirection;
@synthesize arrowOffset = _arrowOffset;

#define kArrowBase 0.0f
#define kArrowHeight 0.0f
#define kBorderInset 0.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (CGFloat) arrovBase
{
    return kArrowBase;
}

+ (CGFloat) arrowHeight
{
    return kArrowHeight;
}

+ (UIEdgeInsets) contentViewInsets
{
    return UIEdgeInsetsMake(kBorderInset, kBorderInset, kBorderInset, kBorderInset);
}

+ (BOOL) wantsDefaultContentAppearance
{
    return YES;
}

@end

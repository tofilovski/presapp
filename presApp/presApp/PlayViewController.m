//
//  PlayViewController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 5/16/14.
//
//

#import "PlayViewController.h"

@interface PlayViewController ()
{
    NSArray *animationImg;
}

@end

@implementation PlayViewController

@synthesize presentationNameLabel;
@synthesize presentationName;
@synthesize presentationDateLabel;
@synthesize presentationDate;
@synthesize presentationImageView;
@synthesize presentationImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    presentationNameLabel.text = presentationName;
    presentationDateLabel.text = presentationDate;
    presentationImageView.image = presentationImage;

    [self imageAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imageAnimation
{
    //animationImg = @[@"angry_birds_cake.jpg", @"creme_brelee.jpg", @"egg_benedict.jpg", @"full_breakfast.jpg", @"green_tea.jpg", @"ham_and_cheese_panini.jpg", @"ham_and_egg_sandwich.jpg", @"hamburger.jpg", @"instant_noodle_with_egg.jpg", @"japanese_noodle_with_pork.jpg", @"mushroom_risotto.jpg", @"noodle_with_bbq_pork.jpg", @"starbucks_coffee.jpg", @"thai_shrimp_cake.jpg", @"vegetable_curry.jpg", @"white_chocolate_donut.jpg"];
    
    animationImg = [NSArray arrayWithObjects:[UIImage imageNamed:@"angry_birds_cake.jpg"], [UIImage imageNamed:@"creme_brelee.jpg"], [UIImage imageNamed:@"ham_and_cheese_panini.jpg"], [UIImage imageNamed:@"japanese_noodle_with_pork.jpg"], [UIImage imageNamed:@"starbucks_coffee.jpg"], [UIImage imageNamed:@"thai_shrimp_cake.jpg"], [UIImage imageNamed: @"white_chocolate_donut.jpg"], nil];

    int numImages = [animationImg count];
    double resfreshRate = 1;
    double desiredFPS = 1/resfreshRate;
    presentationImageView.animationImages = animationImg;
    presentationImageView.animationDuration = numImages/desiredFPS;
    [presentationImageView startAnimating];
    //[presentationImageView stopAnimating];
}

- (IBAction)playButton:(id)sender
{
    //[self startMediaBrowserFromViewController:self usingDelegate:self];
    [self videoPlayer];
}

- (void)videoPlayer
{
    NSString *urlPath = [[NSBundle mainBundle] pathForResource:@"3idiots.mov" ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:urlPath];
    videoPlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    [self.view addSubview:videoPlayer.view];
    videoPlayer.view.frame = CGRectMake(0, 0, 768, 1000);
    [videoPlayer play];
}
/*
- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate{
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentModalViewController: mediaUI animated: YES];
    return YES;
    
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [self dismissModalViewControllerAnimated: YES];
}

// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    [self dismissModalViewControllerAnimated:NO];
    
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef)mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo) {
        
        NSString *moviePath = [[info objectForKey:
                                UIImagePickerControllerMediaURL] path];
        MPMoviePlayerViewController* theMovie =
        [[MPMoviePlayerViewController alloc] initWithContentURL: [info objectForKey:
                                                                  UIImagePickerControllerMediaURL]];
        [self presentMoviePlayerViewControllerAnimated:theMovie];
        
        // Register for the playback finished notification
        [[NSNotificationCenter defaultCenter]
         addObserver: self
         selector: @selector(myMovieFinishedCallback:)
         name: MPMoviePlayerPlaybackDidFinishNotification
         object: theMovie];
        
        
    }
}
// When the movie is done, release the controller.
-(void) myMovieFinishedCallback: (NSNotification*) aNotification
{
    [self dismissMoviePlayerViewControllerAnimated];
    
    MPMoviePlayerController* theMovie = [aNotification object];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver: self
     name: MPMoviePlayerPlaybackDidFinishNotification
     object: theMovie];
    // Release the movie instance created in playMovieAtURL:
}
*/
@end

//
//  PopOverBAckgroundView.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/25/14.
//
//

#import <UIKit/UIKit.h>

@interface PopOverBAckgroundView : UIPopoverBackgroundView

@property (nonatomic, strong) UIImageView *arrowImageView;

//- (UIImage *) drawArrowImage:(CGSize) size;
@end

//
//  PopContentController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/24/14.
//
//

#import "PopContentController.h"
#import "Parse/Parse.h"

@interface PopContentController ()

@end

@implementation PopContentController
{
    NSArray *presentationList;
    NSMutableArray *tempArray;
}

@synthesize tableMenu;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    //NSLog(@"view initWithNibNamed POPCONTENT");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.view.backgroundColor = [UIColor redColor];
        //elf.contentSizeForViewInPopover = CGSizeMake(200, 200);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.calendarData = [NSMutableArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Japanese Noodle with Pork", nil];
    self.favoritesData = [NSMutableArray arrayWithObjects:@"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", nil];
    self.programsData = [NSMutableArray arrayWithObjects:@"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    tempArray = [[NSMutableArray alloc] init];
    [tempArray addObjectsFromArray:self.calendarData];
    [self.tableMenu reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)popupSegChg:(id)sender {
    if (popupSegCtrl.selectedSegmentIndex == 0)
    {
        NSLog(@"Calendar selected ...");
        //[self getData];
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray:self.calendarData];
        [self.tableMenu reloadData];
    }
    if (popupSegCtrl.selectedSegmentIndex == 1)
    {
        NSLog(@"Favorites selected ...");
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray:self.favoritesData];
        [self.tableMenu reloadData];
    }
    if (popupSegCtrl.selectedSegmentIndex == 2)
    {
        NSLog(@"Programs selected ...");
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray:self.programsData];
        [self.tableMenu reloadData];
    }
}
/*
- (void)queryForTable
{
    PFQuery *query = [PFQuery queryWithClassName:@"firstTest"];
    
    [query orderByDescending:@"name"];
    presentationList = [query findObjects];
    NSLog(@"query values %@", [presentationList valueForKey:@"1"]);
}

- (void) getCallback:(NSArray *)objects error:(NSError *)error
{
    if (!error) {
        NSLog(@"The name is: %d", objects.count);
        //NSLog(@"The name is: %@", [objects valueForKey:@"name"]);
        presentationList = objects;
        for (int i = 0; i < presentationList.count; i++) {
            //NSLog(@"value at: %d  value: %u", i, [presentationList indexOfObject:i]);
        }
    }
    else
    {
        NSLog(@"The ERROR is: %@   %@", error, [error userInfo]);
    }
}
- (void)getData
{
    PFQuery *getFromParse = [PFQuery queryWithClassName:@"firstTest"];
    //[getFromParse getObjectInBackgroundWithTarget: target:self selector:@selector(getCallback:error:)];
    [getFromParse findObjectsInBackgroundWithTarget:self selector:@selector(getCallback:error:)];
}*/

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Table view count:  %u", [tempArray count]);
    return [tempArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tempArray objectAtIndex:indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"BLA BLA BLA");
}

@end

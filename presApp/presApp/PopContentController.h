//
//  PopContentController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/24/14.
//
//

#import <UIKit/UIKit.h>

@interface PopContentController : UIViewController
{
    IBOutlet UITableView *tableMenuPop;
    IBOutlet UISegmentedControl *popupSegCtrl;
}
- (IBAction)popupSegChg:(id)sender;
@property (nonatomic, strong) NSMutableArray *calendarData;
@property (nonatomic, strong) NSMutableArray *favoritesData;
@property (nonatomic, strong) NSMutableArray *programsData;
@property (strong, nonatomic) IBOutlet UITableView *tableMenu;

@end

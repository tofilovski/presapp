//
//  MainViewController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import "MainViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "Parse/Parse.h"
#import "PopContentController.h"
#import "PopOverBAckgroundView.h"
#import "AllPresentations.h"
#import "CustomCell.h"
#import "AssetCell.h"
#import "MakePresentation.h"
#import "PlayViewController.h"
#import "KxMenu.h"

@interface MainViewController ()
{
    PopContentController *controller;
    UIPopoverController *popoverController;
    NSMutableArray *slikici;
    NSString *presentationName;
    NSString *presentationDate;
    UIImage *presentationImage;
    UIImage *dragImage;
    UIImageView *dragView;
    CGPoint dragViewStartLocation;
}
@property (nonatomic, weak) UIViewController *currentChildViewController;
@property (nonatomic, strong) UILabel *label;
@end

@implementation MainViewController

@synthesize menuBtn;
@synthesize popOver;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Main view controller ...");
    
    slikici = [NSMutableArray arrayWithObjects:@"angry_birds_cake.jpg", @"creme_brelee.jpg", @"egg_benedict.jpg", @"full_breakfast.jpg", @"green_tea.jpg", @"ham_and_cheese_panini.jpg", @"ham_and_egg_sandwich.jpg", @"hamburger.jpg", @"instant_noodle_with_egg.jpg", @"japanese_noodle_with_pork.jpg", @"mushroom_risotto.jpg", @"noodle_with_bbq_pork.jpg", @"starbucks_coffee.jpg", @"thai_shrimp_cake.jpg", @"vegetable_curry.jpg", @"white_chocolate_donut.jpg", nil];
    
    UIColor * colorBtn = [UIColor colorWithRed:141/255.0f green:179/255.0f blue:68/255.0f alpha:1.0f];
    [[self.saveBtn layer] setBorderWidth:1.5f];
    [[self.saveBtn layer] setBorderColor:colorBtn.CGColor];
    
    [[self.previewBtn layer] setBorderWidth:1.5f];
    [[self.previewBtn layer] setBorderColor:colorBtn.CGColor];
    
    //NSLog(@"DImenzii: %f  %f", _collectionView.frame.size.width, _collectionView.frame.size.height);
	
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(8, 10, 48, 40);
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.menuBtn];
    
    controller = [[PopContentController alloc] initWithNibName:@"PopContentController" bundle:nil];
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:controller];
    self.popOver.popoverBackgroundViewClass = [PopOverBAckgroundView class];
    
    //self.collectionView.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    [self.assetCollectionView registerNib:[UINib nibWithNibName:@"AssetCell" bundle:nil] forCellWithReuseIdentifier:@"assetCell"];
    
    UILongPressGestureRecognizer *longGest = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGestAction:)];
    longGest.minimumPressDuration = 0.7; //sec
    [self.assetCollectionView addGestureRecognizer:longGest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}



- (IBAction)changeViews:(id)sender
{
    if (self.makePresentation.hidden == NO && self.collectionView.hidden == YES)
    {
        self.makePresentation.hidden = YES;
        self.collectionView.hidden = NO;
        self.pageControl.viewForBaselineLayout.hidden = NO;
    }
    else if (self.makePresentation.hidden == YES && self.collectionView.hidden == NO)
    {
        self.makePresentation.hidden = NO;
        self.collectionView.hidden = YES;
        self.pageControl.viewForBaselineLayout.hidden = YES;
    }
}

- (IBAction)openPopUp:(id)sender
{
    if ([self.popOver isPopoverVisible]) {
    }
    else
    {
        [self showPopOver];
    }
}

- (void) showPopOver
{
    
    CGRect poprect = CGRectMake(self.calendarButton.frame.origin.x, self.calendarButton.frame.origin.y, self.calendarButton.frame.size.width, self.calendarButton.frame.size.height);
    self.popOver.popoverContentSize = CGSizeMake(400.0f, 400.0f);
    [self.popOver presentPopoverFromRect:poprect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.popOver dismissPopoverAnimated:NO];
}

//implementation of two or more collection views

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section
{
    if (cv == self.collectionView)
    {
        //NSLog(@"collection view  %u", slikici.count);
        return slikici.count;
    }
    else if (cv == self.assetCollectionView)
        return 12;
    return nil;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"collection view MAIN");
    if (cv == self.collectionView)
    {
        //NSLog(@"  1  ");
        static NSString *identifier = @"CELL";
        //NSLog(@"  2  ");
        CustomCell *cell = [cv dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:  indexPath];
        
        [cell.playBtnOutlet addTarget:self action:@selector(playBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.editBtnOutlet addTarget:self action:@selector(editBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.emailBtnOutlet addTarget:self action:@selector(emailBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.deleteBtnOutlet addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        //NSLog(@"  3  ");
        UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
        //NSLog(@"  4  ");
        recipeImageView.image = [UIImage imageNamed:[slikici objectAtIndex:indexPath.row]];
        //cell.nameLabel.text = [NSString stringWithFormat:@"%@  %ld", cell.nameLabel.text, (long)indexPath.row];
        //NSLog(@"  5  ");
        return cell;
    }
    else if (cv == self.assetCollectionView)
    {
        //NSLog(@"  6  ");
        static NSString *identifier = @"assetCell";
        //NSLog(@"  7  ");
        AssetCell *cell = [cv dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:  indexPath];
        //NSLog(@"  8  ");
        UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:110];
        //NSLog(@"  9  ");
        recipeImageView.image = [UIImage imageNamed:[slikici objectAtIndex:indexPath.row]];
        //NSLog(@"  10  ");
        return cell;

    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (cv == self.collectionView)
    {
        CustomCell *cell = (CustomCell *)[cv cellForItemAtIndexPath:indexPath];
        if (cell.menuView.hidden == YES) {
            cell.menuView.hidden = NO;
        }
        else
            cell.menuView.hidden = YES;
    
        //NSLog(@"Cell touched %@ at indexPath %ld", cell, (long)indexPath.row);
    }
    else if (cv == self.assetCollectionView)
    {
        //AssetCell *assetCell = (AssetCell *)[cv cellForItemAtIndexPath:indexPath];
        //NSLog(@"Asset Cell touched %@ at indexPath %ld", assetCell, (long)indexPath.row);
    }
}

- (void)scrollViewDidScroll:(UICollectionView *)collectionView
{
    CGFloat pageWidth = self.collectionView.frame.size.width;
    int page = floor((self.collectionView.contentOffset.x - pageWidth/2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    //NSLog(@"PAge number: %d", page);
}

-(IBAction)pageSelected:(id)sender
{
    //NSLog(@"Page selected:  %d", self.pageControl.currentPage);
    CGRect frame;
    frame.origin.x = self.collectionView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.collectionView.frame.size;
    [self.collectionView scrollRectToVisible:frame animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"play presentation prepares for segue");
    if ([[segue identifier] isEqualToString:@"playPresentation"])
    {
        /*
        NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
        
        // load the image, to prevent it from being cached we use 'initWithContentsOfFile'
        NSString *imageNameToLoad = [NSString stringWithFormat:@"%d_full", selectedIndexPath.row];
        NSString *pathToImage = [[NSBundle mainBundle] pathForResource:imageNameToLoad ofType:@"JPG"];
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:pathToImage];
        */
        PlayViewController *playViewController = [segue destinationViewController];
        playViewController.presentationName = [NSString stringWithFormat:@"%@", presentationName];
        playViewController.presentationDate = [NSString stringWithFormat:@"%@", presentationDate];
        playViewController.presentationImage = presentationImage;
        NSLog(@"play presentation prepares for segue %@   %@", presentationName, presentationDate);
    }
}

-(void)playBtnPressed:(UIButton *)sender
{
    CGPoint center = CGPointMake(CGRectGetMidX(sender.bounds), CGRectGetMidY(sender.bounds));
    CGPoint transformedCenter = [sender convertPoint:center toView:self.collectionView];
    
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:transformedCenter];
    
    if (indexPath == nil) {
        return;
    }
    
    NSLog(@"play clicked actiom %@", indexPath);
    
    CustomCell *cell = (CustomCell *) [self.collectionView cellForItemAtIndexPath:indexPath];
    presentationName = cell.nameLabel.text;
    presentationDate = cell.dateLabel.text;
    
    presentationImage = cell.imageBackground.image;
    NSLog(@"play clicked actiom %@", cell.nameLabel.text);
    [self performSegueWithIdentifier:@"playPresentation" sender:self];
}

-(void)editBtnPressed:(UIButton *)sender
{
    NSLog(@"edit clicked actiom");
}

-(void)emailBtnPressed:(UIButton *)sender
{
    NSLog(@"email clicked actiom");
}

-(void)deleteBtnPressed:(UIButton *)sender
{
    NSLog(@"delete clicked actiom");
}

- (void)longGestAction:(UILongPressGestureRecognizer *)gestureRecognizer
{
    //CGPoint loc = [gestureRecognizer locationInView:self.assetCollectionView];
    //CGPoint dragViewStartLocation;
    NSIndexPath *startIndex;
    //UIImageView *dragView;
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint loc = [gestureRecognizer locationInView:self.assetCollectionView];
        startIndex = [self.assetCollectionView indexPathForItemAtPoint:loc];
        if (startIndex)
        {
            AssetCell *dragCell = (AssetCell *)[self.assetCollectionView cellForItemAtIndexPath:startIndex];
            dragView = (UIImageView *)[dragCell viewWithTag:110];
            //NSLog(@"image name:  %@  viev name: %@", dragView.image, dragView);
            dragImage = dragView.image;
            
            [dragCell.contentView setAlpha:0.7f];
            [dragView removeFromSuperview];
            [self.makePresentation addSubview:dragView];
            dragView.center = [self.assetCollectionView convertPoint:loc toView:self.makePresentation];//self.view
            dragViewStartLocation = dragView.center;
            [self.makePresentation bringSubviewToFront:dragView];
            
            [UIView animateWithDuration:0.9f animations:^{CGAffineTransform transform = CGAffineTransformMakeScale(1.2f, 1.2f); dragView.transform = transform; }];
            
        }
        return;
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        //NSLog(@" UIGestureRecognizerStateChanged >>>>  %@", dragView);
        if (!dragView)
            return;
        //NSLog(@" 1 >>>>");
        CGPoint location = [gestureRecognizer locationInView:self.collectionView];
        CGPoint translation;
        translation.x = location.x - dragViewStartLocation.x;
        translation.y = location.y - dragViewStartLocation.y;
        CGAffineTransform theTransform = dragView.transform;
        theTransform.tx = translation.x;
        theTransform.ty = translation.y;
        dragView.transform = theTransform;
        //NSLog(@"location %f  %f", location.x, location.y);
        [self.makePresentation bringSubviewToFront:dragView];//self.view
        return;
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        if (dragView)
        {
            //dragView.center = CGPointMake(dragView.center.x, dragView.center.y);
            dragView.center = CGPointMake(dragView.center.x + dragView.transform.tx, dragView.center.y + dragView.transform.ty);
            CGAffineTransform theTransform = dragView.transform;
            theTransform.tx = 0.0f;
            theTransform.ty = 0.0f;
            //UIView *droptarget = [self mapDisplayModelToReceivevView];
            
            //CGPoint pointInDropZone = [gestureRecognizer locationInView:self.dropArea];
            //NSLog(@"dragView in drop :: %f    %f" , dragView.center.x, dragView.center.y);
            //NSLog(@"point in drop :: %f    %f" , pointInDropZone.x, pointInDropZone.y);
            
            //dragView.center = pointInDropZone;
            //[UIView animateWithDuration:0.9f animations:^{dragView.transform = CGAffineTransformIdentity; } completion:^(BOOL finished){ }];
            
            //CGRect convertTargetFrame = [self.makePresentation convertRect:self.dropArea.frame fromView:self.dropArea.superview];
            CGRect introSlide = [self.makePresentation convertRect:self.introSlideView.frame fromView:self.introSlideView.superview];
            CGRect slideOne = [self.makePresentation convertRect:self.slideOneView.frame fromView:self.slideOneView.superview];
            CGRect slideTwo = [self.makePresentation convertRect:self.slideTwoView.frame fromView:self.slideTwoView.superview];
            CGRect slideThree = [self.makePresentation convertRect:self.slideThreeView.frame fromView:self.slideThreeView.superview];
            CGRect slideFour = [self.makePresentation convertRect:self.slideFourView.frame fromView:self.slideFourView.superview];
            CGRect slideFive = [self.makePresentation convertRect:self.slideFiveView.frame fromView:self.slideFiveView.superview];
            CGRect emailSlide = [self.makePresentation convertRect:self.emailSlideView.frame fromView:self.emailSlideView.superview];
            
            
            if (CGRectContainsPoint(introSlide, dragView.center))
            {
                NSLog(@"  introSlide  ");
                //self.slideOneView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(slideOne, dragView.center))
            {
                self.slideOneView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(slideTwo, dragView.center))
            {
                self.slideTwoView.image = dragImage;
                [dragView removeFromSuperview];
                [self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(slideThree, dragView.center))
            {
                self.slideThreeView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(slideFour, dragView.center))
            {
                self.slideFourView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(slideFive, dragView.center))
            {
                self.slideFiveView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else if (CGRectContainsPoint(emailSlide, dragView.center))
            {
                NSLog(@"  emailSlide  ");
                //self.slideOneView.image = dragImage;
                [dragView removeFromSuperview];
                //[self.assetCollectionView reloadData];
            }
            else
            {
                [dragView removeFromSuperview];
            }
            
            dragView = nil;
            [self.assetCollectionView reloadItemsAtIndexPaths:[self.assetCollectionView indexPathsForVisibleItems]];
            [self.assetCollectionView reloadData];
            [self.assetCollectionView layoutIfNeeded];
            return;
        }
    }
    
    /*/NSLog(@"Long Gesture >>>>");
    if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        NSLog(@"UIGestureRecognizerStateChanged ...");
        
        CGPoint location = [gestureRecognizer locationInView:self.assetCollectionView];
        CGPoint translation;
        NSLog(@"location %f  %F", location.x, location.y);
    }
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint gesturePoint = [gestureRecognizer locationInView:self.assetCollectionView];
    
    NSIndexPath *indexPath = [self.assetCollectionView indexPathForItemAtPoint:gesturePoint];
    if (indexPath == nil) {
        NSLog(@"could not find index path");
    }
    else
    {
        UICollectionViewCell *cell = [self.assetCollectionView cellForItemAtIndexPath:indexPath];
        NSLog(@"cell:   %@", cell);
    }*/
}

- (IBAction)saveBtnAction:(id)sender
{
    NSLog(@"SAVE button >>>");
}

- (IBAction)previewBtnAction:(id)sender
{
    NSLog(@"PREVIEW button >>>");
}
@end

//
//  MenuViewController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import "MenuViewController.h"
#import "ECSlidingViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.slidingViewController setAnchorRightRevealAmount:100.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
    
    newButton.tag = 1;
    previewButton.tag = 2;
    programButton.tag = 3;
    favouritesButton.tag = 4;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)newMenu:(id)sender
{
    UIViewController *newViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Main"];
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}
- (IBAction)previewMenu:(id)sender
{
    UIViewController *newViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Second"];
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}
- (IBAction)programsMenu:(id)sender
{
    UIViewController *newViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PictureMenu"];
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}

- (IBAction)favoritesMenu:(id)sender
{
    
    UIViewController *newViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Second"];
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}
- (IBAction)clicked:(id)sender
{
    NSLog(@" somee button clicked");
    UIButton *button = (UIButton *)sender;
    if (button.tag == 1)
    {
        [newButton setEnabled:NO];
    }
}
@end

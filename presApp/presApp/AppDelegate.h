//
//  AppDelegate.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/8/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

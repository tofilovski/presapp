//
//  LoginScreenController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/8/14.
//
//

#import "LoginScreenController.h"
#import "QuartzCore/QuartzCore.h"
#import "Parse/Parse.h"

@interface LoginScreenController ()

@end

@implementation LoginScreenController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIDeviceOrientation orientation = self.interfaceOrientation;
    
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
    
    UIColor * colorField = [UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0f];
    [[userName layer] setBorderWidth:1.5f];
    [[userName layer] setBorderColor:colorField.CGColor];
    
    [[passWord layer] setBorderWidth:1.5f];
    [[passWord layer] setBorderColor:colorField.CGColor];


    UIColor * colorBtn = [UIColor colorWithRed:141/255.0f green:179/255.0f blue:68/255.0f alpha:1.0f];
    [[logInBtn layer] setBorderWidth:1.5f];
    [[logInBtn layer] setBorderColor:colorBtn.CGColor];

    [[signUpBtn layer] setBorderWidth:1.5f];
    [[signUpBtn layer] setBorderColor:colorBtn.CGColor];

    //logInBtn.layer.borderColor = [[UIColor colorWithRed:141/255.0f green:179/255.0f blue:68/255.0f  alpha:0.0f]CGColor];
    //[UIColor colorWithRed:141/255.0f green:179/255.0f blue:68/255.0f alpha:1.0f];
    //logInBtn.layer.borderWidth = 2.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
}

- (IBAction)Login:(id)sender
{
    [PFUser logInWithUsernameInBackground:userName.text password:passWord.text target:self selector:@selector(handleLogin:error:)];
    
}

- (void)handleLogin:(PFUser *)user error:(NSError *)error
{
    if (user) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login succesfull!" message:@"Create your presentation" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        [self performSegueWithIdentifier:@"logInToApp" sender:self];
    }
    else
    {
        NSString *errorString = [error userInfo][@"error"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login unsuccesfull!" message:errorString delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        
    }
}

- (IBAction)SignUp:(id)sender
{
    NSLog(@"SignUp function in");
    [self performSegueWithIdentifier:@"signUpSegue" sender:self];
}

@end

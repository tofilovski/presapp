//
//  ViewController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/8/14.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize progressView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIDeviceOrientation orientation = self.interfaceOrientation; //[[UIDevice currentDevice] orientation];
    //[self willAnimateRotationToInterfaceOrientation:orientation duration:2.0f];
    NSLog(@"Orientation:  %d", orientation);
    
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        NSLog(@"Portrait:  %d", orientation);
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
    {
        NSLog(@"Landscape:  %d", orientation);
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    //CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 0.5f);
    //progressView.transform = transform;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
}
*/
-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"portrait.png"]]];
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"landscape.png"]]];
    }
}

- (IBAction)startCount:(id)sender
{
    self.countDown = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(UpdateUi:) userInfo:nil repeats:YES];
}

- (void)UpdateUi:(NSTimer *)timer
{
    static int count = 0; count++;
    
    if (count <= 10) {
        //NSLog(@"startCount in: %d",count);
        //self.progressLabel.text = [NSString stringWithFormat:@"%d %%", count*10];
        self.progressView.progress = (float)count/10.0f;
        
    }
    else
    {
        [self.countDown invalidate];
        self.countDown = nil;
        
        //LoginViewController *logInViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        //[self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        //[self presentedViewController:logInViewController animated:YES completion:nil];
        
        //LoginViewController *loginVieController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        //[self.navigationController pushViewController:loginVieController animated:YES];
        
        [self performSegueWithIdentifier:@"loginScreen" sender:self];
    }
}

@end

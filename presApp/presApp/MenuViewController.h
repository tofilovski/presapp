//
//  MenuViewController.h
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController
{
    IBOutlet UIButton *newButton;
    IBOutlet UIButton *previewButton;
    IBOutlet UIButton *programButton;
    IBOutlet UIButton *favouritesButton;
}

@end

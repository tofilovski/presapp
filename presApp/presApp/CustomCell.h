//
//  CustomCell.h
//  presApp
//
//  Created by Miroslav Pavlovski on 5/8/14.
//
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageBackground;
@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UIButton *playBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *editBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *emailBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtnOutlet;

- (IBAction)playBtnAction:(id)sender;
- (IBAction)editBtnAction:(id)sender;
- (IBAction)emailBtnAction:(id)sender;
- (IBAction)deleteBtnAction:(id)sender;
@end

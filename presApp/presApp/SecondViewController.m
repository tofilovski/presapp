//
//  SecondViewController.m
//  presApp
//
//  Created by Miroslav Pavlovski on 4/11/14.
//
//

#import "SecondViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "Parse/Parse.h"

@interface SecondViewController ()
@property (nonatomic, strong) UILabel *label;
@end

@implementation SecondViewController

@synthesize menuBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Second view controller ...");
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }

    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(8, 10, 48, 40);
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.menuBtn];
    
    _label = [[UILabel alloc] initWithFrame:self.view.bounds];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.text = @"Copy Me!";
    NSLog(@"1");
    [self.view addSubview:_label];
    NSLog(@"2");
    UILongPressGestureRecognizer *gestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu:)];
    NSLog(@"3");
    [self.view addGestureRecognizer:gestureRecognizer];
    NSLog(@"4");     
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void) getCallback:(PFObject *)getName error:(NSError *)error
{
    if (!error) {
        NSLog(@"The name is: %@", getName[@"prepTime"]);
    }
    else
    {
        NSLog(@"The ERROR is: %@   %@", error, [error userInfo]);
    }
}
- (IBAction)getData:(id)sender
{
    PFQuery *getFromParse = [PFQuery queryWithClassName:@"firstTest"];
    [getFromParse getObjectInBackgroundWithId:@"y2FlXtatvG" target:self selector:@selector(getCallback:error:)];
}


- (CGRect)targetrect
{
    return CGRectMake(self.label.center.x, self.label.center.y, 0.0, 0.0);
}

- (void)showMenu:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"showMenu gestureRecognizer");
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self becomeFirstResponder];
        
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setTargetRect:[self targetrect] inView:self.view];
        [menuController setMenuVisible:YES animated:YES];
    }
}

- (void)copy:(id)sender
{
    NSLog(@"Copy meny button");
}

- (BOOL)canBecomeFirstResponder
{
    NSLog(@"first responder");
    return YES;
}


@end
